# Hugo Builder

This is a Dockerfile that'll output an image that can be used for doing everything needed to update a hugo website when pushed to GitLab.


# Building
`docker build -t registry.gitlab.com/stokholm/hugo-builder:{version here} .`

`docker build -t registry.gitlab.com/stokholm/hugo-builder:latest .`

# Pushing
`docker push registry.gitlab.com/stokholm/hugo-builder:{version here}`

`docker push registry.gitlab.com/stokholm/hugo-builder:latest`
