FROM golang:1.15-buster

RUN apt-get update && \
    apt-get install -y \
        python3 \
        python3-pip \
        python3-setuptools \
        groff \
        less \
    && pip3 install --upgrade pip \
    && apt-get clean

RUN pip3 --no-cache-dir install --upgrade awscli

RUN	wget https://github.com/larrabee/s3sync/releases/download/1.13/s3sync_1.13_Linux_x86_64.tar.gz \
    && tar -xzf s3sync_1.13_Linux_x86_64.tar.gz \
    && rm s3sync_1.13_Linux_x86_64.tar.gz \
    && cp s3sync /go/bin/s3sync

COPY hugo /go/bin/hugo